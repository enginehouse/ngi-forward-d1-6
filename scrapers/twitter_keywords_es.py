# Spain
# Environment, Sustainability and Resilience
umb0='''crisis climática tecnología
calentamiento global tecnología
"right to repair"
"derecho a reparar"
cambio climático tecnología
tecnologia sustentable
sustentabilidad tecnologia'''.split('\n')

# Decentralising Power and Building Alternatives
umb1='''open source
competencia tecnología
descentralización tecnología
tecnología descentralizada
tecnologías descentralizadas'''.split('\n')

# Public Space and Sociality
umb2='''smart city
espacio publico tecnología
espacio publico Internet
espacio publico tecnologias'''.split('\n')

# Privacy, Identity, and Data Governance (no dato de governancia)
umb3='''privacidad en línea
privacidad de datos
data governance
información personal tecnología
información personal Internet
identificación digital
e-id
cifrado en línea
anonimato en línea'''.split('\n')

# Trustworthy Information Flows, Cybersecurity and Democracy
umb4='''noticias falsas tecnología
noticias falsas Internet
fake news tecnología
fake news Internet
filter bubble
democracia tecnología
democracia Internet
censura online
censura Internet
censura tecnología'''.split('\n')

# Access, Inclusion and Justice
umb5='''discriminación en línea
tecnología ética
libertad en línea
igualdad Internet
igualdad tecnología
derechos humanos tecnología
"open internet"
tecnología inclusiva
igualdad en línea
justicia tecnología'''.split('\n')

end_time='2021-04-01T00:00:00z'
start_time='2018-01-01T00:00:00z'
max_results='500'
