import json
from time import sleep
from theguardian import theguardian_section
from theguardian import theguardian_content
import pandas as pd
import ast
from apikey import API_KEY

def run_scraping(start_date):
    YOUR_API_KEY=API_KEY

    # get the tech sections
    headers = {"q": "technology"}  # q=query parameter/search parameter
    section = theguardian_section.Section(api=YOUR_API_KEY, **headers)

    # get the results
    section_content = section.get_content_response()
    results = section.get_results(section_content)

    # get different editions from the results
    editions = results[0]['editions']

    # get uk/technology edition apiUrl
    uk_tech = [edi["apiUrl"] for edi in editions if edi["id"] == "uk/technology"][0]

    headers= {"from-date": start_date, 'show-blocks':'body', 'url':uk_tech}
    # use this api url to sports content
    content = theguardian_content.Content(api=YOUR_API_KEY, **headers)

    # get section response
    content_response = content.get_content_response()

    result = content.get_results(content_response)

    pages=content_response['response']['pages']

    all_results=[]
    for i in range(pages):
        headers= {"from-date": start_date, 'show-blocks':'body', 'url':uk_tech, 'page':i+1}
        # use this api url to sports content
        content = theguardian_content.Content(api=YOUR_API_KEY, **headers)
        # get section response
        content_response = content.get_content_response()
    #   data = content_response.json()
        all_results.extend(content_response['response']['results'])
        sleep(.5)
        if i % 20 == 0:
            print('results page: '+str(i))

    len(all_results)

    df=pd.DataFrame(all_results)

    df.to_csv('./guardian_articles.csv')
