# Polish

# Environment, Sustainability and Resilience
umb0='''kryzys klimatyczny technologia
kryzys klimatyczny technologie
kryzys klimatyczny internet
globalne ocieplenie technologia
globalne ocieplenie technologie
globalne ocieplenie internet
"right to repair"
"prawo do naprawy"
zmiany klimatyczne technologia
zmiany klimatyczne internet
"zrównoważony rozwój" technologia
zrównoważone technologie'''.split('\n')

# Decentralising Power and Building Alternatives
umb1='''open source
konkurencja technologia
decentralizacja technologia
zdecentralizowane technologie
zdecentralizowana technologia'''.split('\n')

# Public Space and Sociality
umb2='''smart city
"przestrzeń publiczna" technologia
"przestrzeń publiczna" internet'''.split('\n')

# Privacy, Identity, and Data Governance
umb3='''prywatność dane
prywatność w sieci
prywatność internet
prywatność technologia
dane osobowe
digital id
cyfrowe id
e-id
szyfrowanie
anonimowość w sieci
anonimowość internet'''.split('\n')

# Trustworthy Information Flows, Cybersecurity and Democracy
umb4='''fake news technologia
fake news sieć
fake news internet
fake news internecie
fake news technologie
bańce informacyjnej
bańki informacyjne
bańka informacyjna
filter bubble
demokracja technologia
demokracja internet
cenzura internet
cenzura w sieci
demokratyczne technologie'''.split('\n')

# Access, Inclusion and Justice
umb5='''dyskryminacja internet
dyskryminacja technologia
dyskryminacja w sieci
etyczne technologie
etyka technologia
etyka internet
wolność w sieci
wolność internet
prawa człowieka internet
prawa człowieka technologia
sprawiedliwość internet
sprawiedliwość technologia'''.split('\n')

fields='attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,reply_settings,source,text,withheld'
end_time='2021-04-01T00:00:00z'
start_time='2018-01-01T00:00:00z'
max_results='500'
