// var margin = { top: 40, right: 150, bottom: 60, left: 30 },
//     width = 1200 - margin.left - margin.right,
//     height = 800 - margin.top - margin.bottom;
 // set the dimensions and margins of the graph

// append the svg object to the body of the page
d3.select("svg").remove();
d3.select(".tooltip").remove();


function transform_headline(hl, link) {
    let split_str = ' / '
    let spl = hl.split(split_str);
    let websites = spl[0];

    let img_website = ''
    for (const website of websites.split(', ')) {
        img_website += '<img height="24px" src="/static/img/' + website + '_logo.png" alt="' + website + '"></img> '
    }
    let link_sh = link.replace(
        'http://www.', '').replace('https://www.', '').replace('http://', '').replace('https://', '').split('/')[0]

    return img_website + ' / ' + spl.slice(1, spl.length).join(split_str) + ' (' + link_sh + ')';
}


function create_d3(
    type,
                annotations,
                data,
                settings_dict,
                keywords_dict,
                margin_values) {
    // const parseTime = d3.timeParse("%d-%b-%y")
    // const timeFormat = d3.timeFormat("%d-%b-%y")

    let margin = margin_values;
    let width = 1200 - margin.left - margin.right;
    let height = 800 - margin.top - margin.bottom;
    console.log('mv', margin_values);

    var svg = d3.select("#my_dataviz")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

    //Skipping setting domains for sake of example
    const xx = d3.scaleTime().range([0, 800])
    const yy = d3.scaleLinear().range([300, 0])

    const makeAnnotations = d3.annotation()
        .editMode(false)
        //also can set and override in the note.padding property
        //of the annotation object
        .notePadding(5)
        .type(type)
        //accessors & accessorsInverse not needed
        //if using x, y in annotations JSON
        .accessors({
            xx: d => x(parseTime(d.date)),
            yy: d => y(d.close)
        })
        .accessorsInverse({
            date: d => timeFormat(x.invert(d.xx)),
            close: d => y.invert(d.yy)
        })
        .annotations(annotations)

    d3.select("svg")
        .append("g")
        .attr("class", "annotation-group")
        .call(makeAnnotations)


    // ---------------------------//
    //       AXIS  AND SCALE      //
    // ---------------------------//

    // Add X axis
    var x = d3.scaleLinear()
        .domain([-80, 80])
        .range([0, width]);
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .attr("class", "axis_main")
        .call(d3.axisBottom(x));

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([-80, 80])
        .range([height, 0]);
    svg.append("g")
        .attr("class", "axis_main")
        .call(d3.axisLeft(y));

    // Add a scale for bubble size
    var z = d3.scaleSqrt()
        .domain([200000, 1310000000])
        .range([2, 30]);

    // Add a scale for bubble color
    var myColor = d3.scaleOrdinal()
        .domain(Object.keys(settings_dict))
        .range([d3.rgb('#C24A65'), d3.rgb('#3DBDBE'), d3.rgb('#AAE4E4'), d3.rgb('#E0A3B0'),
            d3.rgb('#6C4AC2'), d3.rgb('#7EBF3D'), d3.rgb('#C7E3AA'), d3.rgb('#B5A4E0'),
            d3.rgb('#4AC2A8'), d3.rgb('#BF3D3D'), d3.rgb('#E3AAAA'), d3.rgb('#A4E0D3'),
            d3.rgb('#A0C24A'), d3.rgb('#7E3DBF'), d3.rgb('#C7AAE3'), d3.rgb('#CFE0A4'),
        ]);
    // .range(d3.schemeCategory10); # d3.rgb('#3C3950'), d3.rgb('#585A5B'),

    // ---------------------------//
    //      TOOLTIP               //
    // ---------------------------//

    // -1- Create a tooltip div that is hidden by default:
    var tooltip = d3.select("#my_title")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("border-radius", "5px")
        .style("padding", "10px")
        .style("color", "black")

    // -2- Create 3 functions to show / update (when mouse move but stay on same circle) / hide the tooltip
    var showTooltip = function(d) {
        console.log('showTooltip', d);
        tooltip
            .transition()
            .duration(200)
        tooltip
            .style("opacity", 1)
            .html(transform_headline(d['headline'], d['link']))
            // .style("left", (d3.mouse(this)[0]+30) + "px")
            // .style("top", (d3.mouse(this)[1]+30) + "px")
            .style("border", '3px solid')
            .style("border-color", myColor(d['topic']))
    }
    var moveTooltip = function(d) {
        tooltip
        // .style("left", (d3.mouse(this)[0]+30) + "px")
        // .style("top", (d3.mouse(this)[1]+30) + "px")
    }
    var hideTooltip = function(d) {
        tooltip
            .transition()
            .duration(200)
            .style("opacity", 0)
    }

    // ---------------------------//
    //      KEYWORDS TOOLTIP               //
    // ---------------------------//

    // -1- Create a tooltip div that is hidden by default:
    var kwtooltip = d3.select("#my_kw")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("border-radius", "5px")
        .style("padding", "10px")
        .style("color", "black")

    // -2- Create 3 functions to show / update (when mouse move but stay on same circle) / hide the tooltip
    var showkwTooltip = function(d) {
        kwtooltip
            .transition()
            .duration(200)
        kwtooltip
            .style("opacity", 1)
            // .html('<table class="table"><thead><th>Keywords</th><tbody><tr><td>' + keywords_dict[d['topic']].join('</td></tr><tr><td>') + '</td></tr></tbody></table>')
            .html('<table class="table"><thead><th>' + settings_dict[d['topic']] + '</th><tbody><tr><td>' + keywords_dict[d['topic']].join('</td></tr><tr><td>') + '</td></tr></tbody></table>')

            // .style("left", (d3.mouse(this)[0] + 300) + "px")
            // .style("top", (d3.mouse(this)[1] + 300) + "px")
            .style("border", '3px solid')
            .style("border-color", myColor(d['topic']))
    }
    var movekwTooltip = function(d) {
        kwtooltip
            // .style("left", (d3.mouse(this)[0] + 300) + "px")
            // .style("top", (d3.mouse(this)[1] + 300) + "px")
    }
    var hidekwTooltip = function(d) {
        kwtooltip
            .transition()
            .duration(200)
            .style("opacity", 0)
    }


    // ---------------------------//
    //       HIGHLIGHT GROUP      //
    // ---------------------------//

    // What to do when one group is hovered
    var highlight = function(d) {
        // reduce opacity of all groups
        d3.selectAll(".bubbles").style("opacity", .05)
        // expect the one that is hovered
        console.log('highlight', d['topic'].replace(' ', '_'));
        d3.selectAll(".bubbles" + d['topic'].replace(' ', '_')).style("opacity", 1)
    }

    // And when it is not hovered anymore
    var noHighlight = function(d) {
        d3.selectAll(".bubbles").style("opacity", 0.2)
    }

    var gridLinesX = svg.append("g")
        .selectAll("line")
        .data(x.ticks(10).slice(1, -1))
        .enter().append("line")
        .attr("class", "grid-line")
        .attr("x1", d => x(d))
        .attr("y1", d => 0)
        .attr("x2", d => x(d))
        .attr("y2", d => 1400);

    var gridLinesY = svg.append("g")
        .selectAll("line")
        .data(y.ticks(10).slice(1, -1))
        .enter().append("line")
        .attr("class", "grid-line")
        .attr("x1", d => 0)
        .attr("y1", d => y(d))
        .attr("x2", d => 1000)
        .attr("y2", d => y(d));

    // ---------------------------//
    //       CIRCLES              //
    // ---------------------------//

    function openlink(d) {
        console.log(d);
        window.open(d['link']);
    };

    var dotmouseover = function(d) {
        console.log('dotmouseover', d);
        showTooltip(d);
        showkwTooltip(d);
    };

    var dotmousemove = function(d) {
        moveTooltip(d);
        movekwTooltip(d);
    };

    var dotmouseleave = function(d) {
        hideTooltip(d);
        hidekwTooltip(d);
    };

    // Add dots
    svg.append('g')
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("class", function(d) { return "bubbles bubbles" + d.topic.replace(' ', '_') })
        .attr("cx", function(d) { return x(d.x); })
        .attr("cy", function(d) { return y(d.y); })
        .attr("r", function(d) { return 3; })
        .style("fill", function(d) { return myColor(d['topic']); })
        // -3- Trigger the functions for hover
        .on("mouseover", dotmouseover)
        .on("mousemove", dotmousemove)
        .on("mouseleave", dotmouseleave)
        // .on("mouseover", showTooltip )
        // .on("mousemove", moveTooltip )
        // .on("mouseleave", hideTooltip )
        .on("click", function(d) { openlink(d); })
}

load_map = function(category, margin_values = false) {
    if (margin_values === false) {
        var margins = { top: 120, right: 150, bottom: 60, left: 150 };
    }
    else {
        var margins = margin_values;
    }
    console.log(category);
    //Read the data
    d3.csv("../out/" + category + "_d3.csv", function(data) {
        console.log(data);

        d3.json("../out/" + category + "_d3_settings.json", function(err, settings) {
            console.log(err);
            console.log("settings");
            console.log(settings);
            settings_dict = {};
            keywords_dict = {};
            for (var i = 0; i < settings['main'].length; i++) {
                settings_dict[settings['main'][i]['topic']] = settings['main'][i]['name'];
                keywords_dict[settings['main'][i]['topic']] = settings['main'][i]['keywords'];
            }
            console.log(settings_dict);

            // ANNOTATIONS //
            const type = d3.annotationCallout

            //Add annotations
            const annotations = settings['annotations']

            create_d3(
                type,
                annotations,
                data,
                settings_dict,
                keywords_dict,
                margins
                )
        })
    })
}
