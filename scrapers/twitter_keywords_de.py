# German
umb0='''Klimakrise Technologie
"right to repair"
"Recht auf Reparatur"
nachhaltige Technologie
Nachhaltigkeit Technologie'''.split('\n')

# Decentralising Power and Building Alternatives (no Marktwettbewerb Technologie)
umb1='''open source
Wettbewerb Technologie
Dezentralisierung Technologie
dezentrale Technologie'''.split('\n')

# Public Space and Sociality
umb2='''"smart city"
öffentlicher Raum Technologie
öffentlicher Raum Internet'''.split('\n')

# Privacy, Identity, and Data Governance (no Datenamt)
umb3='''Online-Datenschutz
Datenschutz Internet
Datenschutz Technologie
persönliche Daten Technologie
persönliche Daten Internet
digitale ID
Online-Verschlüsselung
Verschlüsselung Internet
Verschlüsselung Technologie
Online-Anonymität
Anonymität Internet
e-id'''.split('\n')

# Trustworthy Information Flows, Cybersecurity and Democracy
umb4='''fake news Technologie
fake news Internet
filter bubble
Demokratie Technologie
Demokratie Internet
Online-Zensur
Zensur Internet'''.split('\n')

# Access, Inclusion and Justice
umb5='''Online Diskriminierung
ethische Technologie
Online Freiheit
Freiheit Technologie
Gleichberechtigung Technologie
Menschenrechte Technologie
open internet
inklusive Technologie
Justiz Technologie'''.split('\n')
