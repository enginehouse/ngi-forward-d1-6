# Portuguese

# Environment, Sustainability and Resilience
umb0='''crise climatica tecnologia
aquecimento global tecnologia
"right to repair"
"direito de reparar"
das Alterações Climáticas tecnologia
tecnologia sustentável'''.split('\n')

# Decentralising Power and Building Alternatives
umb1='''open source
concorrência tecnologia
descentralização tecnologia
tecnologia descentralizada'''.split('\n')

# Public Space and Sociality
umb2='''smart city
"espaço público" tecnologia
"espaço público" internet'''.split('\n')

# Privacy, Identity, and Data Governance
umb3='''privacidade online
gestão de dados
"dados pessoais"
dados privados
criptografia online
e-id
anonimato online'''.split('\n')

# Trustworthy Information Flows, Cybersecurity and Democracy
umb4='''fake news online
fake news tecnologia
filter bubble
democracia tecnologia
democraticamente tecnologia
censura online'''.split('\n')

# Access, Inclusion and Justice
umb5='''discriminação online
tecnologia ética
liberdade online
liberdade tecnologia
igualdade tecnologia
direitos humanos tecnologia
direitos humanos online
open internet
justiça tecnologia'''.split('\n')

end_time='2021-04-01T00:00:00z'
start_time='2018-01-01T00:00:00z'
max_results='500'
