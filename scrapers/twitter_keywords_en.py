
## query keywords for the collection of tweets related to the umbrella topics
## example query='"climate crisis" tech lang:en -is:retweet -is:reply has:links -has:media'

# Environment, Sustainability and Resilience

umb0='''"climate crisis" tech
"climate crisis" technology
"global warming" tech
"global warming" technology
"right to repair"
"climate change" tech
"climate change" technology
sustainable tech
sustainable technology
sustainability tech
sustainability technology'''.split('\n')


# Decentralising Power and Building Alternatives 
umb1='''open source tech
open source technology
competition tech
competition technology
decentralization technology
decentralization tech
decentralized technology
decentralized tech
decentralised tech
decentralised technology
decentralisation tech
decentralisation technology'''.split('\n')

# Public Space and Sociality
umb2='''"public space" technology
"public space" tech
"smart city"'''.split('\n')

# Privacy, Identity, and Data Governance
umb3='''"online privacy"
"data privacy"
"data governance"
"personal data"
"digital id"
"e-id" 
online encryption
online anonymity
online privacy'''.split('\n')

# Trustworthy Information Flows, Cybersecurity and Democracy
umb4='''online "fake news"
"filter bubble"
democracy tech
democracy technology
democratically technology
democratically tech
online censorship'''.split('\n')

# Access, Inclusion and Justice
umb5='''online discrimination
ethical technology
ethical tech
online freedom
freedom technology
freedom tech
equality technology
equality tech
human rights tech
human rights technology
"open internet"
inclusive technology
inclusive tech
online equality
justice tech
justice technology
human rights online'''.split('\n')
