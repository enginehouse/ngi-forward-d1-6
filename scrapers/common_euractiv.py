from common_scraping import sleep_r, full_driver, csv_dir_common, delta
import pandas as pd
from selenium.common.exceptions import TimeoutException, WebDriverException, NoSuchElementException

csvdir = csv_dir_common()

driver = full_driver()

site='https://www.euractiv.com/sections/digital/'

def get_links():
  arts_links=[]
  for i in range(20):
      driver.get(site+'page/'+str(i+1))
      print('page: '+str(i))
      links=driver.find_element_by_class_name('ea-archive-body').find_elements_by_class_name('caption')
      arts_links.append([art.find_element_by_tag_name('a').get_attribute('href') for art in links])
      sleep_r('s')
  return arts_links

links=get_links()
links=[item for sublist in links for item in sublist]

def get_info(links):
    topic_list = []
    for i, link in enumerate(links):
        link_dict = {}
        driver.get(link)
        sleep_r('s')
        try:
            driver.find_element_by_id('modal_output_corona').find_element_by_class_name('close').click()
        except:
            pass
        try:
            link_dict['author']=driver.find_element_by_class_name('ea-byline').text
        except:
            link_dict['author']=np.nan
        link_dict['title']=driver.find_element_by_class_name('ea-post-title').find_element_by_tag_name('h1').text
        link_dict['date']=driver.find_element_by_class_name('ea-dateformat').text
        link_dict['link']=link

        article_p_list = []
        for p in driver.find_element_by_class_name('ea-article-body-content').find_elements_by_tag_name('p'):
            article_p_list.append(p.text)
            link_dict['text'] = '\n\n'.join(article_p_list)

        topic_list.append(link_dict)
        print(i)
        sleep_r('s')

    return(topic_list)

arts=get_info(links)
df=pd.DataFrame(arts)
df.drop_duplicates(subset=['title'], inplace=True)
print(df)
df.to_csv(csvdir + 'euractiv.csv')
