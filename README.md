# NGI Forward: Open data sets, code and methodology
## Deliverable D1.6
### Results website: https://fwd.delabapps.eu/ 

## TL;DR
- In this repository you will find 5 tutorials which enable you to play around with the methodology we have created in the [NGI Forward](https://fwd.delabapps.eu/) project
- All the analyses are based on the articles published under the tech category of The Guardian in the period 2016-01-01:2020-01-02
- To collect the articles, you need to register for an API key: https://open-platform.theguardian.com
- 0_text_transformation_guide.ipynb is for advanced users who may wish to tweak functions. Users who would like to produce results based on existing methods can proceed to the next tutorials.
- 1_initial_transformation.ipynb collects and prepares the data in the suitable format for the text mining analysis conducted in tutorials A, B1 and B2
- A_topic_identification.ipynb will guide you step-by-step through the text mining pipeline we have developed for identifying trending technologies and related social issues in tech media. This tutorial covers:
    - identification of trending keywords using OLS regression 
    - analysis of keywords co-occurrences 
    - sentiment of trending topics using VADER package
- B1_LDA.ipynb presents how to prepare the topic modelling analysis on the dataset of news articles using a popular LDA algorithm
- B2_t-SNE shows how to combine LDA analysis from B1 with t-SNE in order to map clusters of articles
- C_t-SNE shows how to reproduce what DELab has done in its topic mapping exercise on tech articles: https://ngitopics.delabapps.eu/

# Repository structure short


```
.
├── 0_text_transformation_guide.ipynb
├── 1_initial_transformation.ipynb
├── A_topic_identification.ipynb
├── B1_LDA.ipynb
├── B2_t-SNE.ipynb
├── C_t-SNE.ipynb
├── LICENSE.md
├── README.md
├── guardian_api.py
├── imposing-tests
├── imposing_functions.py
├── main_settings.py
├── requirements.txt
├── scrapers
├── screenshots
└── theguardian

```


- "0_text_transformation_guide.ipynb" is a quick explanation of how text transformation functions work in NGI Forward project.
- "1_initial_transformation.ipynb" collects and prepares the data in the suitable format for the text mining analysis conducted in tutorials A, B1 and B2
- "A_topic_identification.ipynb" will guide you step-by-step through the text mining pipeline we have developed for identifying trending technologies and related social issues in tech media. This tutorial covers:
    - identification of trending keywords using OLS regression 
    - analysis of keywords co-occurrences 
    - sentiment of trending topics using VADER package
- "B1_LDA.ipynb" presents how to prepare the topic modelling analysis on the dataset of news articles
- "B2_t-SNE.ipynb" shows how to combine LDA analysis from B1 with t-SNE in order to map clusters of articles
- C_t-SNE shows how to reproduce what DELab has done in its topic mapping exercise on tech articles: https://ngitopics.delabapps.eu/
- "guardian_api.py" code used for communication with the Guardian API in "1_initial_transformation.ipynb"
- "imposing_functions.py" defines all the functions required in the tutorials. Documentation is available in 0_text_transformation_guide.ipynb
- "main_settings.py" sets paths and major parameters used in the imposing_functions_py, i.e. output repositories, analysed time period, important columns of the input dataset, SVD dimensions and t-SNE perplexity values
- "out" folder is created in "1_initial_transformation.ipynb" and contains output files created in all tutorials (e.g. keywords frequencies, co-occurrences, topic modelling visualization etc.). We don't include it at Gitlab because of the large size (>1GB)
- "requirements.txt" lists all the necessary packages, install them using "pip install -r requirements.txt" command in your terminal
- "scrapers" folder contains scrapers enabling harvesting articles we have used in our analysis. In particular you will find guardian_api.py code instructing how to retrieve articles from The Guardian API
- "screenshots": images used in the repo
- "theguardian" folder contains functions enabling articles retrieval using the Guardian API

# Repository structure detailed
Repository structure after running all the tutorials
```
.
├── 0_text_transformation_guide.ipynb
├── 1_initial_transformation.ipynb
├── A_topic_identification.ipynb
├── B1_LDA.ipynb
├── B2_t-SNE.ipynb
├── C_t-SNE.ipynb
├── LICENSE.md
├── README.md
├── imposing_functions.py
├── main_settings.py
├── out
│   ├── assets_all
│   │   ├── counts.json
│   │   └── months.pickle
│   ├── mod_all
│   │   ├── dfs_articles2016-01guardian.csv
│   │   ├── dfs_articles2016-02guardian.csv
│   │   ├── dfs_articles2016-03guardian.csv
│   │   ...
│   └── res_all
│       ├── coefs_weighted_site.csv
│       ├── coefs_weighted_site12.csv
│       ├── coefs_weighted_site3.csv
│       ├── coefs_weighted_site6.csv
│       ├── cooc_weighted.csv
│       ├── data_all.pickle
│       ├── filt
│       ├── freq-all_site.csv
│       ├── freq_2016-01-all_site.csv
│       ├── freq_2016-02-all_site.csv
│       ├── freq_2016-03-all_site.csv
│       ...
│       ├── guardian_X_svd.pickle
│       ├── guardian_df.pickle
│       ├── guardian_embedding_300_25_pca.pickle
│       ├── guardian_embedding_300_25_random.pickle
│       ├── guardian_embedding_300_pca.pickle
│       ├── guardian_embedding_300_random.pickle
│       ├── guardian_word_indices.pickle
│       ├── lda_tuning_results.csv
│       ├── model.gensim
│       ├── model.gensim.expElogbeta.npy
│       ├── model.gensim.id2word
│       ├── model.gensim.state
│       ├── model.html
│       ├── sentiments_cooc_mod.csv
│       └── sentiments_mod.csv
├── requirements.txt
├── scrapers
│   ├── common_fastcompany.py
│   ├── common_ieee.py
│   ├── common_politico.py
│   ├── common_register.py
│   ├── common_reuters.py
│   ├── common_scraping.py
│   ├── common_techforge.py
│   ├── common_techforge_blockchain.py
│   └── news_api.py
├── screenshots
│   ├── bubbles.png
│   ├── coocs.png
│   ├── ngi_logo.png
│   ├── pillars.png
│   └── tsne.png
└── theguardian
    ├── LICENSE
    ├── __init__.py
    ├── theguardian_content.py
    ├── theguardian_edition.py
    ├── theguardian_section.py
    └── theguardian_tag.py

```
### NGI Forward focuses on identifying and evaluating the key enabling technologies and topics that will underpin the Next Generation Internet.

#### NGI Forward's three key pillars:
![pillars](screenshots/pillars.png)
=====
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Our analyses are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

Guardian API usage is licensed under GNU General Public License v3.0
