# libraries
import json
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from dateutil.parser import parse
from datetime import datetime
from datetime import timedelta
import pandas as pd
import numpy as np
import re
from selenium.common.exceptions import NoSuchElementException
from common_scraping import *

news_xpath_list = ['//div[@class= "c-entry-box--compact c-entry-box--compact--article"]',
                   '//div[@class= "c-entry-box--compact c-entry-box--compact--featured c-entry-box--compact--hero"]',
                   '//div[@class= "c-entry-box--compact c-entry-box--compact--article c-entry-box--compact--hero"]',
                   '//div[@class= "c-entry-box--compact c-entry-box--compact--feature"]',
                   '//div[@class="c-compact-river__entry "]']

news_link_xpath = ['.//a[@data-analytics-link]']
pagination_xpath = '//a[contains(@class,"c-pagination")]'

title_xpath = '//h1[@class="c-page-title"]|//h2[contains(@class, "title")]'
date_xpath = '//time'
author_xpath = '//span[@class="c-byline__item"]'
article_text_xpath = '//p[@id]'


# ectract date from
def extract_date(url):
    date = re.findall(r'/(\d{4})/(\d{1,2})/(\d{1,2})/', url)
    if len(date) == 0:
        return False
    date = '-'.join([i for i in date[0]])
    return date


# convert date to datetime format for comparison
def convert_date(date):
    converted_date = datetime.strptime(date, '%Y-%m-%d')
    return (converted_date)


# collect links from one page
def collect_links(news_xpath=news_xpath_list, news_link_xpath=news_link_xpath, extract_date=extract_date):
    all_news_items = []  # list with all news blocks
    links = []
    # gather news_items since there are different xpathes
    for news_xpath in news_xpath_list:
        n = driver.find_elements_by_xpath(news_xpath)
        all_news_items.extend(n)

    for news in all_news_items:
        news_link = news.find_element_by_xpath(news_link_xpath[0]).get_attribute('href')
        news_date = extract_date(news_link)
        if news_date is not False:
            links.append({'link': news_link,
                          'date': news_date})
        else:
            print('not added', news_link)
    return links


# go through the pagination and collect all the links with news till the "compare_date"
def pagination(collect_links, pagination_xpath, url, category, convert_date):
    confirmed_links = []
    url = url
    driver.get(url)
    time.sleep(2)
    driver.maximize_window()
    try:
        cookies = driver.find_element_by_xpath('//div[@class="m-privacy-consent__button-content"]')
        time.sleep(1)
        cookies.click()
    except NoSuchElementException:
        print("Cookies are not found")
    time.sleep(5)
    compare_date = pd.to_datetime('now') - delta()

    article_date = compare_date

    while article_date >= compare_date:
        collected_links = collect_links()
        print('links0', collected_links[-1]['date'], len(collected_links), len(confirmed_links))
        time.sleep(1)
        for link in collected_links:
            if convert_date(link['date']) > compare_date and link['link'] not in [i['link'] for i in all_links]:
                link['category'] = category
                confirmed_links.append(link.copy())
        article_date = convert_date(collected_links[-1]['date'])

        next_pages = driver.find_elements_by_xpath(pagination_xpath)
        if len(next_pages) > 0:
            next_page = next_pages[-1]
            time.sleep(2)
            ActionChains(driver).move_to_element(next_page).click(next_page).perform()
            time.sleep(2)
        else:
            while True:
                try:
                    driver.find_element_by_xpath('//button[contains(@class,"load-more__button")]').click()
                except NoSuchElementException:
                    break
                time.sleep(1)

            collected_links = collect_links()
            print('links1', collected_links[-1]['date'], len(collected_links))

            time.sleep(1)
            for link in collected_links:
                if convert_date(link['date']) > compare_date and link['link'] not in [i['link'] for i in all_links]:
                    link['category'] = category
                    confirmed_links.append(link.copy())

            year, month = int(driver.current_url.split('/')[-2]), int(driver.current_url.split('/')[-1])
            print(year, month, len(confirmed_links))
            month -= 1
            if month == 0:
                month = 12
                year -= 1
            driver.get('https://www.theverge.com/archives/tech/{0}/{1}'.format(year, month))


    with open('verge_links.json', 'w+') as f:
        json.dump(confirmed_links, f)
    return confirmed_links


# download news
news = []
news_dict = {}


def download_news(title_xpath, date_xpath, author_xpath, article_text_xpath, topic):
    for l in all_links:
        try:
            driver.get(l['link'])
        except Exception as ex:
            print('error', l['link'], ex)
            continue
        print(l['link'])
        time.sleep(1)
        news_dict['link'] = l['link']
        news_dict['category'] = l['category']
        try:
            news_dict['title'] = driver.find_element_by_xpath(title_xpath).text
        except NoSuchElementException:
            print('no title', l['link'])
            news_dict['title'] = np.nan
        try:
            n_date = driver.find_element_by_xpath(date_xpath).get_attribute("datetime")
            n_date = n_date.split('T')[0]
            news_dict['date'] = n_date
        except NoSuchElementException:
            print('no date', l['link'])
            news_dict['date'] = l['date']
        try:
            n_author = driver.find_element_by_xpath(author_xpath).text
            n_author = n_author.split('@')[0]
            news_dict['author'] = n_author
        except NoSuchElementException:
            print('no author', l['link'])
            news_dict['author'] = ''
        article_par_list = []
        for p in driver.find_elements_by_xpath(article_text_xpath):
            article_par_list.append(p.text)
        news_dict['article_text'] = '\n\n'.join(article_par_list)
        news.append(news_dict.copy())
        news_df = pd.DataFrame.from_dict(news, orient='columns')
        news_df.to_csv(csv_dir_common() + 'verge_' + topic + '.csv')
    print(news_df)


topics = {
    'Tech': 'https://www.theverge.com/tech',
    'Science': 'https://www.theverge.com/science'
}

driver = full_driver()
all_links = []
for key, value in topics.items():
    all_links = pagination(collect_links=collect_links, pagination_xpath=pagination_xpath, url=value, category=key,
                                convert_date=convert_date)

    download_news(title_xpath=title_xpath,
                  date_xpath=date_xpath,
                  author_xpath=author_xpath,
                  article_text_xpath=article_text_xpath,
                  topic=key)
