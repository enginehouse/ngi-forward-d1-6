# this guide is for collecting data using the service of Event Registry 
# you need to set up a free account at http://eventregistry.org
# it enables you to collect articles from the last 30 days

import pandas as pd

#generate a key via the website and include here
API_KEY='api key'

from eventregistry import *
er = EventRegistry(apiKey = API_KEY)

# the following code will collect articles published by the Guardian from a few days
# you can also choose more sources - then the code will iterate by source
sources=['guardian']
for source in sources:
	print(er.getNewsSourceUri(source))
	
##the documentation for the service can be found here: https://github.com/EventRegistry/event-registry-python

	
all_articles=[]
for source in sources:
	q = QueryArticlesIter(
		sourceUri=er.getNewsSourceUri(source), 
	dateStart=datetime.datetime(2020,3,15),
	dateEnd=datetime.datetime(2020,3,12))
	print(source)
	for article in q.execQuery(er, sortBy = "date", 
							   returnInfo=ReturnInfo(articleInfo = ArticleInfoFlags(
								   categories = True, location = True, concepts=True, links=True,
								   videos=True, image=True, socialScore=True, sentiment=True, dates=True,
								   extractedDates=True, originalArticle = True, storyUri=True))):
		articles_source.append(article)
		all_articles.append(article)
		results_df=pd.DataFrame.from_dict(articles)
 