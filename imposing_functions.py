import pandas as pd
import numpy as np
import pickle
from datetime import datetime
from collections import Counter, defaultdict
from ast import literal_eval
import gc
import os
import sys
import string
from nltk import FreqDist  # frequency distribution
from nltk.tokenize import sent_tokenize  # fast tokenize
from nltk.tokenize.treebank import TreebankWordTokenizer, TreebankWordDetokenizer
from nltk.tokenize.punkt import PunktSentenceTokenizer
from nltk.stem import SnowballStemmer  # stemming
from gensim.models.phrases import Phraser, Phrases  # automatic bigrams
from sklearn import linear_model  # OLS
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer  # sentiment analysis
from pympler import muppy, summary


def memory_test():
    gc.collect()
    all_objects = muppy.get_objects()
    sum1 = summary.summarize(all_objects)
    summary.print_(sum1)


def make_directory(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)


def flatten(l):
    return [item for sublist in l for item in sublist]


def tz(date):
    try:
        return date.tz_localize('UTC')
    except TypeError:
        return date.tz_convert('UTC')


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


class DirectoryIterator(object):
    def __init__(self, dirname, text_token_column):
        self.dirname = dirname
        self.text_token_column = text_token_column
        self.count = 0

    def __iter__(self):
        for fname in sorted(os.listdir(self.dirname)):
            lines = pd.read_csv(self.dirname + fname, index_col=0)[self.text_token_column].apply(literal_eval)
            print('file', fname, 'read', self.count)
            self.count += 1
            lines = [a for b in lines for a in b]
            lines = [[i for i in x if i.isalnum()] for x in lines]
            for line in lines:
                yield line


class Enginehouse:
    def __init__(self, date_start, date_end, assets_dir, mod_dir, res_dir, weights,
                 weight_title=1, weight_first_paragraph=1):
        self.text_column = 'body'
        self.text_column_paragraph = self.text_column + '_paragraph'
        self.text_column_token = self.text_column + '_token'
        self.text_column_token_lower = self.text_column_token + '_lower'
        self.text_column_token_stemmed = self.text_column_token + '_stemmed'
        self.text_column_token_count = self.text_column_token + '_count'

        self.link_column = 'url'
        self.site_column = 'site'
        self.author_column = 'authors'
        self.date_columns = ('dateTime', 'date_outside')
        self.title_columns = ('title', 'title_outside', 'webTitle')
        self.date_time_column = 'dateTime'
        self.period_column = 'year_month'
        self.date_column = 'full_date'

        self.periods_filename = 'months.pickle'
        self.dfs_articles = 'dfs_articles'

        self.date_start = pd.to_datetime(date_start).tz_localize('UTC')
        self.date_end = pd.to_datetime(date_end).tz_localize('UTC')
        self.assets_dir = assets_dir
        self.mod_dir = mod_dir
        self.res_dir = res_dir
        self.weight_title = weight_title
        self.weight_first_paragraph = weight_first_paragraph

        self.weights = weights

        self.most_significant = 25000
        self.min_coef_norm = 0.025
        self.min_coef_norm_count = 0.0125

    def create_bigrams(self):
        dd = DirectoryIterator(self.mod_dir, self.text_column_token)
        phrases = Phrases(dd)
        return Phraser(phrases)

    @staticmethod
    def add_bigrams(x):
        new_x = {}
        for k, v in x.items():
            k2 = k.split('_')
            if len(k2) == 1:
                if k in new_x:
                    new_x[k] += v
                else:
                    new_x[k] = v
            else:
                for k3 in k2:
                    if k3 in new_x:
                        new_x[k3] += v
                    else:
                        new_x[k3] = v
                new_x[k] = v
        return new_x

    def final_modify(self):
        # to lower, stem
        st = SnowballStemmer('english')
        for filename in sorted(os.listdir(self.mod_dir)):
            art = pd.read_csv(self.mod_dir + filename, index_col=0)
            art[self.text_column_token] = art[self.text_column_token].apply(literal_eval)
            art[self.text_column_token_lower] = art[self.text_column_token].apply(
                lambda x: [[word.lower() for word in paragraph] for paragraph in x])
            art[self.text_column_token_stemmed] = art[self.text_column_token_lower].apply(
                lambda x: [['_'.join([st.stem(i) for i in word.split('_')]) for word in paragraph] for paragraph in x])
            art[self.text_column_token_count] = art[self.text_column_token_stemmed].apply(lambda x: dict(
                Counter(FreqDist(flatten(x))))).apply(self.add_bigrams)
            art.to_csv(self.mod_dir + filename)
            print('final', filename, 'done')

    def bigramize(self, bigram):
        for filename in sorted(os.listdir(self.mod_dir)):
            art = pd.read_csv(self.mod_dir + filename, index_col=0)
            art[self.text_column_token] = art[self.text_column_token].apply(
                lambda x: [bigram[y] for y in literal_eval(x)])
            art.to_csv(self.mod_dir + filename)
            print('bigramize', filename, 'done')

    def most_used(self, period, site):
        """Return dict with counts, and the number of articles in a given dataframe.
        Save ngram counts to mod directory.
        """
        time_start = datetime.now()
        art = pd.read_csv(os.path.join(self.mod_dir, self.dfs_articles + period + site + '.csv'), index_col=0)
        div = art.shape[0]  # number of articles

        # sum all the tokens
        all_frequencies = Counter()
        for token in art[self.text_column_token_count].apply(literal_eval):
            all_frequencies.update(token)

        print(datetime.now() - time_start, art.shape[0], period, site)
        del art
        gc.collect()
        return all_frequencies, div

    def save_frequencies(self, method):
        """Save unweighted frequencies and counts by month for all sites"""
        months = pd.read_pickle(os.path.join(self.assets_dir, self.periods_filename))
        site_counter = defaultdict(lambda: 0)
        for i_month, month in enumerate(months):
            for i_site, site in enumerate(self.weights.keys()):  # weights contain sites as keys
                word_counter, div = self.most_used(month, site)
                site_counter[site] += div
                word_df = pd.DataFrame.from_dict(dict(word_counter), orient='index')

                # we need counts and frequencies
                word_df.columns = ['count_' + month + site]
                dd = div
                if method == 'count':
                    dd = word_df.loc[word_df.index.map(
                        lambda x: len(str(x).split('_')) == 1)]['count_' + month + site].sum().sum() / 100
                word_df['freq_' + month + site] = word_df['count_' + month + site] / dd
                if i_site == 0:
                    word_dfs_all = word_df.copy()
                else:
                    word_dfs_all = word_dfs_all.join(word_df, how='outer')  # join with previous sites
                print(word_dfs_all.shape)
                del word_df
                gc.collect()
            word_dfs_all.to_csv(self.res_dir + 'freq{}_'.format(method) + month + '-all_site.csv')
            del word_dfs_all
            gc.collect()
        pd.Series(site_counter).to_json(self.assets_dir + 'counts.json')

    def save_concat(self, method, any_month_used=2):
        """Concat weighted frequencies and unweighted counts for all sites and all months"""
        months = pd.read_pickle(os.path.join(self.assets_dir, self.periods_filename))
        final_df = pd.DataFrame()
        for i_month, month in enumerate(sorted(months, reverse=True)):
            final_columns = []
            frequency_month = pd.read_csv(os.path.join(
                self.res_dir, 'freq{}_'.format(method) + month + '-all_site.csv'), index_col=0)
            frequency_month['freq_' + month] = 0
            frequency_month['count_' + month] = 0
            final_columns.extend(['freq_' + month, 'count_' + month])

            # sum counts and weighted frequencies
            for site, site_weight in self.weights.items():
                frequency_month['freq_' + month + site].fillna(0, inplace=True)
                frequency_month['count_' + month + site].fillna(0, inplace=True)
                frequency_month['freq_' + month] += frequency_month['freq_' + month + site] * site_weight
                frequency_month['count_' + month] += frequency_month['count_' + month + site]

            if i_month < any_month_used:
                # take only these words which occured at least once in the last any_month_used months
                final_df = final_df.join(frequency_month[final_columns], how='outer')
            else:  # do not add further words
                final_df = final_df.join(frequency_month[final_columns], how='left')
            # there may be some empty rows, which cause problems later
            final_df = final_df.loc[final_df.index.notnull()]
            print(final_df.shape)
            os.remove(os.path.join(self.res_dir, 'freq{}_'.format(method) + month + '-all_site.csv'))
            del frequency_month
            gc.collect()

        final_df.to_csv(os.path.join(self.res_dir, 'freq{}-all_site.csv'.format(method)))
        del final_df
        gc.collect()

    def reg(self, method, regression_periods_length=('', 12, 6, 3)):
        """Save regression coefficients to file"""
        periods = pd.read_pickle(os.path.join(self.assets_dir, self.periods_filename))
        regression = linear_model.LinearRegression()
        for regression_length in regression_periods_length:
            print(regression_length)
            df = pd.read_csv(os.path.join(self.res_dir, 'freq{}-all_site.csv'.format(method)), index_col=0)
            df = df[[x for x in df.columns if 'count' not in x]]
            df = df.transpose()  # nice table, easier to manipulate, but requires a lot of memory
            word_list = df.columns  # list of words in columns due to transpose
            df['_period_'] = [str(x) for x in range(len(periods), 0, -1)]  # how many months
            if type(regression_length) == int:
                df = df.loc[df['_period_'].astype(int) > len(periods) - regression_length]
            df.fillna(0, inplace=True)  # regression doesn't work with nans, replace with 0
            coef_normalized, coef_normalized_max, coef = dict(), dict(), dict()

            # regression for every relevant word
            time_old = datetime.now()
            for i_word, word in enumerate(word_list):
                if i_word % 1000 == 0:
                    print(i_word, len(word_list), datetime.now() - time_old)  # progress
                    time_old = datetime.now()

                word_df = df.loc[:, word]
                regression.fit(df.loc[:, '_period_'].values.reshape(-1, 1),
                               word_df.values.reshape(-1, 1))
                coef_normalized[word] = regression.coef_[0][0] / word_df.mean()
                coef_normalized_max[word] = regression.coef_[0][0] / word_df.max()
                coef[word] = regression.coef_[0][0]

            df = df.transpose()  # transpose back, words from columns to indices
            df['coef_norm'], df['coef_norm_max'], df['coef'] = \
                pd.Series(coef_normalized), pd.Series(coef_normalized_max), pd.Series(coef)
            df.sort_values('coef', ascending=False).to_csv(
                self.res_dir + 'coefs{}_weighted_site'.format(method) + str(regression_length) + '.csv')

    def words_with_means(self, coefs):
        """Create dictionary, where keys are words and values are average frequencies"""
        row_words_means = {}
        # n most significant ngrams
        for i, row in coefs[:self.most_significant].iterrows():
            row_words_means[i] = np.mean(row[[x for x in coefs.columns if x.startswith('freq_')]])
        return row_words_means

    def read_articles(self, month, site, cooc_use=True):
        """Read given articles and rename column to format required by cooc() if cooc_use"""
        df_art = pd.read_csv(os.path.join(self.mod_dir, 'dfs_articles' + month + site + '.csv'))
        if cooc_use:
            df_art[self.text_column_token_count] = df_art[
                self.text_column_token_count].apply(lambda x: defaultdict(lambda: 0, literal_eval(x)))
        return df_art

    def count_words(self, art, words, site, row_words_means, article_word_count):
        """Count how many words (and how often) exist in articles on a site"""
        cooc_words = {}

        for word in words:
            cooc_words[word + '_count_' + site] = defaultdict(lambda: 0)
            cooc_words[word + '_bool_' + site] = defaultdict(lambda: 0)
            column_word_count = art[self.text_column_token_count].apply(lambda x: x[word])
            article_word_count[word][site]['_count_'] += sum(column_word_count)
            column_word_bool = column_word_count.apply(bool)
            article_word_count[word][site]['_bool_'] += sum(column_word_bool)

            df_word_exists = art.loc[column_word_count > 0]
            counters = {'count': Counter(), 'bool': Counter()}
            for d in df_word_exists[self.text_column_token_count].values:
                counters['count'].update(d)
                counters['bool'].update({k: (1 if v > 0 else 0) for k, v in d.items()})
            for row_word in row_words_means.keys():
                for counter_name, counter_value in counters.items():
                    cooc_words[word + '_' + counter_name + '_' + site][row_word] = counter_value[row_word]

        return cooc_words, article_word_count

    def create_word_count(self, words):
        """Create dictionary with booleans: sum([1 if exists] else 0) and counts of words"""
        article_word_count = {}
        for word in words:
            article_word_count[word] = {site: {'_bool_': 0, '_count_': 0} for site in self.weights.keys()}
        return article_word_count

    def normalize_by_article_count(self, words, cooc_words, article_word_count):
        """Normalizes cooc DataFrame so that maximum bool (i.e. co-occurrence of words A and A) is 100.
        Analogous procedure is done for counts; however resulting values may be greater than 100,
        if a row-word is used more often than the column-word."""
        old_datetime = datetime.now()
        for i, word in enumerate(words):
            old_datetime = datetime.now()
            for site in self.weights.keys():
                for bc in ['_count_', '_bool_']:
                    if word + bc + site not in cooc_words.columns:
                        continue
                    if article_word_count[word][site][bc] > 0:
                        cooc_words[word + bc + site + '_freq'] = 100 * cooc_words[
                            word + bc + site] / article_word_count[word][site][bc]
                    else:
                        cooc_words[word + bc + site + '_freq'] = 0
        return cooc_words

    def normalize_by_weights(self, words, cooc_all, row_words_means):
        """Return DataFrame with six additional coefficients:
        frequency, frequency normalized by mean, frequency normalized by root of mean
        both for booleans and counts"""
        old_datetime = datetime.now()
        for i, word in enumerate(words):
            old_datetime = datetime.now()
            for bc in ['_count_', '_bool_']:
                cooc_all[word + bc + 'freq_weighted'] = 0
                cooc_all[word + bc + 'freq_weighted_normalized'] = 0
                cooc_all[word + bc + 'freq_weighted_normalized_root'] = 0
                for site, weight in self.weights.items():
                    if word + bc + site + '_freq' in cooc_all.columns:  # avoid KeyErrors
                        cooc_all[word + bc + 'freq_weighted'] += weight * cooc_all[
                            word + bc + site + '_freq']
                        cooc_all[word + bc + 'freq_weighted_normalized'] += weight * cooc_all[
                            word + bc + site + '_freq']
                        cooc_all[word + bc + 'freq_weighted_normalized_root'] += weight * cooc_all[
                            word + bc + site + '_freq']
                for row_word, mean in row_words_means.items():
                    cooc_all.loc[
                        row_word, word + bc + 'freq_weighted_normalized'
                    ] = cooc_all.loc[row_word, word + bc + 'freq_weighted_normalized'] / mean
                    cooc_all.loc[row_word, word + bc + 'freq_weighted_normalized_root'
                    ] = cooc_all.loc[row_word, word + bc + 'freq_weighted_normalized_root'] / (
                            mean ** (1 / 2))

        return cooc_all

    def cooc(self, words_all, method, suffix=''):
        """Save files with co-occurrence values"""
        cooc_to_concat = []
        for i_words, words in enumerate(chunks(words_all, 10000)):
            months = pd.read_pickle(os.path.join(self.assets_dir, self.periods_filename))
            # coefficients for rows
            coefs_compare = pd.read_csv(
                os.path.join(self.res_dir, 'coefs{}_weighted_site.csv'.format(method)), index_col=0)
            if method == 'article':
                coefs_compare = coefs_compare.loc[coefs_compare['coef_norm'] > self.min_coef_norm].sort_values(
                    'coef', ascending=False)
            elif method == 'count':
                coefs_compare = coefs_compare.loc[coefs_compare['coef_norm'] > self.min_coef_norm_count].sort_values(
                    'coef', ascending=False)

            row_words_means = self.words_with_means(coefs_compare)
            cooc_all = []
            article_word_count = self.create_word_count(words)
            old_datetime = datetime.now()
            for site in self.weights.keys():
                print(site, datetime.now() - old_datetime)
                old_datetime = datetime.now()
                cooc_months = []
                for i_month, month in enumerate(months):
                    print(i_month, month)
                    # take articles for columns and rows
                    df_art = self.read_articles(month=month, site=site)

                    cooc_site, article_word_count = self.count_words(
                        df_art, words, site, row_words_means, article_word_count)

                    cooc_months.append(cooc_site)

                    del df_art
                    gc.collect()

                cooc_site = defaultdict(lambda: defaultdict(lambda: 0))
                for cooc_month in cooc_months:
                    for k, v in cooc_month.items():
                        for k1, v1 in v.items():
                            cooc_site[k][k1] += v1

                cooc_all.append(pd.DataFrame.from_dict(cooc_site))

            cooc_all = pd.concat(cooc_all, axis=1)
            cooc_all = self.normalize_by_article_count(words, cooc_all, article_word_count)
            cooc_all = self.normalize_by_weights(words, cooc_all, row_words_means)

            cooc_to_concat.append(cooc_all[[x for x in cooc_all.columns if 'weighted' in x]])
            cooc_all[[x for x in cooc_all.columns if 'weighted' in x]].to_csv(
                os.path.join(self.res_dir, 'cooc{}_weighted_new{}{}.csv'.format(method, i_words, suffix)))
            pd.concat(cooc_to_concat, axis=1).to_csv(
                os.path.join(self.res_dir, 'cooc{}_weighted_newall{}.csv'.format(method, suffix)))

    def comparison_cooc(self, method, n=200, suffix=''):
        """Get dictionary with n most commonly co-occurring stemmed words for a given word."""
        cooc_words = defaultdict(lambda: [])
        cooc_data = pd.read_csv(self.res_dir + 'cooc{}_weighted_newall{}.csv'.format(method, suffix), index_col=0)
        for i in cooc_data.columns:
            if 'count_freq_weighted' in i and '_normalized' not in i:
                column_term = i.split('_count_freq_weighted')[0]
                row_terms = cooc_data[i].sort_values(ascending=False).index[:n].tolist()
                cooc_words[column_term] = row_terms
        return cooc_words

    def sentiment(self, terms, method, suffix=''):
        months = pd.read_pickle(os.path.join(self.assets_dir, self.periods_filename))
        d = TreebankWordDetokenizer()
        coefs = pd.read_csv(self.res_dir + 'coefs{}_weighted_site.csv'.format(method), index_col=0)
        # terms = {term: [x for x in coefs.index if x == term or term in str(x).split('_')] for term in terms}
        terms = {term: [term] + [x for x in coefs.index if term in str(x).split('_')] for term in terms}
        reverse_terms = {}
        for k, v in terms.items():
            for v1 in v:
                if v1 in reverse_terms:
                    reverse_terms[v1].append(k)
                else:
                    reverse_terms[v1] = [k]
        comparison = self.comparison_cooc(method=method, suffix=suffix)

        analyzer = SentimentIntensityAnalyzer()
        all_scores = {}
        all_words = defaultdict(lambda: defaultdict(lambda: []))
        for month in months:
            print(month, datetime.now())

            articles_months = []
            for site in self.weights.keys():
                df_original = self.read_articles(month, site)
                df = self.read_articles(month, site)
                texts = list(zip(df[self.text_column_token_stemmed].tolist(),
                                 df[self.text_column_token_stemmed].apply(lambda x: set(
                                     flatten(list(terms.values()))) & set(flatten(literal_eval(x)))),
                                 df_original[self.text_column_token].tolist()))
                texts = [x for x in texts if x[-1]]  # get rid of articles not containing any term
                if articles_months:
                    articles_months.extend(texts)
                else:
                    articles_months = texts

            print('articles read', datetime.now(), len(articles_months))
            polarities = defaultdict(lambda: [])
            i_articles_months = 0
            old = datetime.now()
            previous_data = ''
            for tokens_original, words, original_unigrams in articles_months:
                if i_articles_months % 500 == 0:
                    print(i_articles_months, datetime.now())
                time = old - datetime.now()
                if time.total_seconds() > 1:
                    print(i_articles_months, time, previous_data)
                previous_data = (len(tokens_original), len(words), len(original_unigrams))
                old = datetime.now()
                i_articles_months += 1
                tokens_original = literal_eval(tokens_original)
                original_unigrams = literal_eval(original_unigrams)
                words_groups = {word: reverse_terms[word] for word in words}
                final_words = {}
                for k, v in words_groups.items():
                    for v1 in v:
                        if v1 in final_words.keys():
                            final_words[v1].append(k)
                        else:
                            final_words[v1] = [k]
                for i in range(len(tokens_original)):
                    for word_reverse, words_to_remove in final_words.items():
                        # print(word_reverse, words_to_remove)
                        to_remove = [[j] for j, x in enumerate(tokens_original[i]) if x in words_to_remove]
                        tokens = list(np.delete(original_unigrams[i], flatten(to_remove)))
                        # print(tokens)
                        sentence = d.detokenize(tokens).replace(' .', '.')
                        vs = analyzer.polarity_scores(sentence)
                        polarities[word_reverse].append(vs['compound'])
                        polarities[word_reverse + '_count'].append(1)
                        for token in set(flatten(tokens_original)) & set(comparison[word_reverse]):
                            if word_reverse != token:
                                to_remove = [[j] for j, x in enumerate(tokens_original[i]) if x in words_to_remove + [token]]
                                tokens = list(np.delete(original_unigrams[i], flatten(to_remove)))
                                # print('diff {} {}\n'.format(word_reverse, token), original_unigrams[i], '\n', tokens)
                                sentence = d.detokenize(tokens).replace(' .', '.')
                                vs = analyzer.polarity_scores(sentence)
                                all_words[word_reverse][token].append(vs['compound'])
                                all_words[word_reverse + '_count'][token].append(1)

            all_scores[month] = {k: sum(v) if k[-6:] == '_count' else v for k, v in polarities.items()}
            print(len(all_scores[month]))
            del articles_months[:]
            gc.collect()

        word_scores = {}
        for month, v in all_scores.items():
            for word, scores in v.items():
                if word not in word_scores.keys():
                    word_scores[word] = {}
                word_scores[word][month] = np.mean(scores)
        cooc_scores = {}
        for word1, v in all_words.items():
            cooc_scores[word1] = {}
            for word2, scores in v.items():
                if word1[-6:] != '_count':
                    cooc_scores[word1][word2] = np.mean(scores)
                else:
                    cooc_scores[word1][word2] = np.sum(scores)

        pd.DataFrame(word_scores).to_csv(self.res_dir + 'sentiments{}_mod{}.csv'.format(method, suffix))
        pd.DataFrame(cooc_scores).to_csv(self.res_dir + 'sentiments{}_cooc_mod{}.csv'.format(method, suffix))


class Transform(Enginehouse):
    def __init__(self, df, site,
                 date_start, date_end, assets_dir, mod_dir, res_dir, weights,
                 weight_title=1, weight_first_paragraph=1):
        Enginehouse.__init__(self, date_start, date_end, assets_dir, mod_dir, res_dir, weights,
                             weight_title, weight_first_paragraph)
        self.df = df
        self.site = site

    def initial_transform(self):
        """ Transform scraped data by dropping duplicates and cleaning dates.
        Initial transformation required because scraped data is quite messy.
        """
        self.df.drop_duplicates(subset=[self.text_column], inplace=True)

        for columns in [self.title_columns, self.date_columns]:
            if len(columns) > 1:
                for column in columns[1:]:
                    if column in self.df.columns:
                        self.df[columns[0]].fillna(self.df[column], inplace=True)

        self.df[self.date_columns[0]] = self.df[self.date_columns[0]].apply(
            lambda x: str(x).replace(' p.m.', '').replace('BUSINESS NEWS', '').split(' | ')[0])
        self.df[self.date_columns[0]] = self.df[self.date_columns[0]].apply(
            lambda x: tz(pd.to_datetime(x)))
        self.df[self.text_column] = self.df.apply(lambda row: '\n\n'.join(self.weight_title * [str(
            row[self.title_columns[0]])]) + '\n\n' + '\n\n'.join(self.weight_first_paragraph * [str(
            row[self.text_column]).split('\n\n')[0]]) + '\n\n' + '\n\n'.join(str(
            row[self.text_column]).split('\n\n')[1:]), axis=1)
        print('pre-drop', self.df.shape, self.site)
        self.df = self.df.loc[
            (self.df[self.date_columns[0]] >= self.date_start) &
            (self.df[self.date_columns[0]] < self.date_end)]
        print('post-drop', self.df.shape, self.site)
        self.df[self.site_column] = self.site

    def months_columns(self, weeks=False):
        """Sort dataframe by date_time_column and create a column with periods (months, assuming YYYY-MM-DD)"""
        if weeks is False:
            self.df[self.period_column] = self.df[self.date_time_column].astype(str).str[0:7]
            print(pd.DataFrame(self.df[self.period_column].value_counts()).reset_index().sort_values('index'))
            self.df[self.date_column] = self.df[self.date_time_column].astype(str).str[0:10]
            self.df.sort_values(self.date_time_column, inplace=True)
        else:
            start = pd.to_datetime('2020-01-01 00:00:00+00:00')
            print(self.df[self.date_time_column])
            self.df[self.period_column] = self.df[self.date_time_column].apply(pd.to_datetime).apply(lambda x: '{}-w{:02}'.format(x.year,
            	(x - start).days // 7))
            self.df[self.date_column] = self.df[self.date_time_column].astype(str).str[0:10]
            self.df.sort_values(self.date_time_column, inplace=True)

    def save_months(self):
        """Create file with list of months."""
        all_months = self.df[self.period_column].unique()
        all_months = all_months[~pd.isnull(all_months)]
        pickle.dump(all_months, open(os.path.join(self.assets_dir, self.periods_filename), 'wb'))

    def tokenize_gram(self):
        """Tokenize and join commonly co-occurring words"""
        remain_columns = [self.text_column,
                          self.author_column, self.site_column, self.link_column, self.period_column, self.date_column]
        remain_columns = [x for x in remain_columns if x in self.df.columns]
        tt = TreebankWordTokenizer()
        st = PunktSentenceTokenizer()

        memory_test()
        print('=== start tokenize_gram', sys.getsizeof(self.df))
        # for x in [col for col in self.df.columns if col not in remain_columns]:
        #     del self.df[x]
        # gc.collect()
        self.df = self.df.loc[:, remain_columns]  # we don't need more columns
        # self.df[self.text_column] = self.df[self.text_column].apply(
        #     lambda x: x.replace('“', '').replace('”', '').replace('’', '\''))
        self.df[self.text_column] = self.df[self.text_column].apply(lambda x: x.replace('—', ' '))
        punct = string.punctuation + '«»…—’‘“”–•'
        punct = punct.replace('.', '')
        self.df[self.text_column] = self.df[self.text_column].apply(
            lambda x: str(x).translate(str.maketrans('', '', punct))
        )
        self.df[self.text_column_paragraph] = self.df[self.text_column].apply(lambda x: x.split('\n\n'))
        memory_test()
        print('=== start tokenize', sys.getsizeof(self.df))
        # sent_tokenize tokenizes by paragraphs
        self.df[self.text_column_token] = self.df[self.text_column_paragraph].apply(
            lambda x: [st.tokenize(y) for y in x])
        memory_test()
        print('=== start flatten', sys.getsizeof(self.df))
        #self.df[self.text_column_token] = self.df[self.text_column_token].apply(
        #    lambda x: [flatten([tt.tokenize(z) for z in y]) for y in x])
        self.df[self.text_column_token] = self.df[self.text_column_token].astype('object')
        self.df.reset_index(inplace=True, drop=True)
        for i, row in self.df.iterrows():
            if i % 1000 == 0:
                print(i, self.df.shape, datetime.now(), 'flatten tokenize')
            self.df.at[i, self.text_column_token] = [flatten(
                [tt.tokenize(z) for z in y]) for y in row[self.text_column_token]]
        print('tokenize done')
        # memory_test()
        gc.collect()
        for period, df_period in self.df.groupby(self.period_column):
            print('save', period)
            df_period.to_csv(self.mod_dir + self.dfs_articles + period + self.site + '.csv')

    def link_to_site_name(self, links_column='url', site_column='site'):
        """DataFrame contains a column with links, transform it to site name"""
        self.df[site_column] =self. df[links_column]
        self.df[site_column] = self.df[site_column].str.replace('://(www\.)?', '').str.replace(
            'https|http', '').str.split('\.com|\.net|\.eu').apply(lambda x: x[0])
        self.df[site_column] = self.df[site_column].apply(lambda x: x.split('.')[1] if '.gizmodo' in x else x)
        self.df[site_column] = self.df[site_column].apply(lambda x: 'ieee' if 'spectrum.ieee.org' in x else x)
        self.df[site_column] = self.df[site_column].apply(lambda x: 'register' if (
                x == 'theregister' or 'theregister.co.uk/' in x) else x)
        print(self.df[site_column].value_counts())
