import requests
import json
import numpy as np
import pandas as pd
import datetime
from time import sleep
import glob
import os
from newspaper import Article
import newspaper
import unidecode
from langdetect import detect

# Useful twitter-api documentation
# Query doc: https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query

# param doc: https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference/get-tweets-search-recent#tab2

bearer_token="" # add account's details
search_url = "https://api.twitter.com/2/tweets/search/all"
# specify which fields you want to retrieve
fields='attachments,author_id,conversation_id,created_at,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,reply_settings,source,text,withheld'

# Functions authorizing your call and connecting you to the twitter api
def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers


def connect_to_endpoint(url, headers, params):
    response = requests.request("GET", search_url, headers=headers, params=params)
#     print(response.status_code)
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.json()


def main():
    headers = create_headers(bearer_token)
    json_response = connect_to_endpoint(search_url, headers, query_params)
    #print(json.dumps(json_response, indent=4, sort_keys=True))
    return json_response

# Keywords for German language query. You can reproduce our other queries simply by copying the keywords from files `twitter_keywords_[country_code].py`

# Environment, Sustainability and Resilience (globale Erwärmung Technologie no tweets)

# German
umb0='''Klimakrise Technologie
"right to repair"
"Recht auf Reparatur"
nachhaltige Technologie
Nachhaltigkeit Technologie'''.split('\n')

# Decentralising Power and Building Alternatives (no Marktwettbewerb Technologie)
umb1='''open source
Wettbewerb Technologie
Dezentralisierung Technologie
dezentrale Technologie'''.split('\n')

# Public Space and Sociality
umb2='''"smart city"
öffentlicher Raum Technologie
öffentlicher Raum Internet'''.split('\n')

# Privacy, Identity, and Data Governance (no Datenamt)
umb3='''Online-Datenschutz
Datenschutz Internet
Datenschutz Technologie
persönliche Daten Technologie
persönliche Daten Internet
digitale ID
Online-Verschlüsselung
Verschlüsselung Internet
Verschlüsselung Technologie
Online-Anonymität
Anonymität Internet
e-id'''.split('\n')

# Trustworthy Information Flows, Cybersecurity and Democracy
umb4='''fake news Technologie
fake news Internet
filter bubble
Demokratie Technologie
Demokratie Internet
Online-Zensur
Zensur Internet'''.split('\n')

# Access, Inclusion and Justice
umb5='''Online Diskriminierung
ethische Technologie
Online Freiheit
Freiheit Technologie
Gleichberechtigung Technologie
Menschenrechte Technologie
open internet
inklusive Technologie
Justiz Technologie'''.split('\n')

# Specify time range and max results, in case of >500 results we iterate over the results
end_time='2021-04-01T00:00:00z'
start_time='2018-01-01T00:00:00z'
max_results='500'

# Query for all keywords in a given umbrella topic for a given language
path='' # e.g.'./germany/umb4_democracy/'

for keyw in umb0: # here specify the umbrella topic you want to query [umb0-umb5]
    next_page=True
    query='{} lang:de -is:retweet -is:reply has:links -has:media'.format(keyw)
    df=pd.DataFrame()
    query_params = {'query': query,'tweet.fields':fields, 'end_time':end_time,'start_time':start_time, 'max_results':max_results}
    j=0
    while next_page is True:
        try:
            json_response=main()
        except:
            counter=0
            while counter<3:
                sleep(30)
                try:
                    json_response=main()
                    counter=3
                except:
                    counter=counter+1
        df=df.append(pd.DataFrame(json_response['data']))
        print('df:'+str(len(df)))
        print(df['created_at'][-1:])
        if 'next_token' in json_response['meta'].keys() and json_response['meta']['result_count']!=0:
                    next_page=True
                    next_token=json_response['meta']['next_token']
                    sleep(2)
                    query_params = {'query': query,'tweet.fields':fields, 'end_time':end_time,'start_time':start_time, 'max_results':max_results,
                                    'next_token':next_token,
                                    }
        else:
            next_page=False
        if len(df)>100000:
            df.to_csv(path+query.split('lang')[0].strip()+'_'+str(j)+'.csv')
            j=j+1
            df=pd.DataFrame()
        if j==5:
            next_page=False

    df.to_csv(path+query.split('lang')[0].strip()+'_'+str(j)+'.csv')

all_files = glob.glob(path + "/*.csv")

li = []

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0,lineterminator='\n')
    li.append(df)

dfm = pd.concat(li, axis=0, ignore_index=True)
dfm.to_csv(path+'umb0_all.csv') 
